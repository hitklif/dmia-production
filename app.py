import pandas as pd
import numpy as np
from catboost import CatBoostRegressor
from flask import Flask, request, render_template
import pickle
from password_strength import PasswordStats

# index взят из репо https://gitlab.com/production-ml/password_app/-/blob/master/templates/index.html

app = Flask(__name__)

features = ['123Seq', '321Seq', 'qweSeq', 'qaSeq', 'wsSeq',
       'pasSeq', 'lovSeq', 'abcSeq', 'youSeq', 'countZero', 'countOne',
       'countTwo', 'countA', 'countE', 'countI', 'countO', 'countN', 'countR',
       'countS', 'sequencesLength', 'repeatedPatternsLength', 'weaknessFactor',
       'entropy', 'charCount', 'numerics']

def feature_generator(input_pass):
    df = pd.DataFrame(columns=['Password'])
    df['Password'] = [input_pass]
    # длина пароля и кол-во уникальных символов и доля уникальных символов
    df['123Seq'] = df['Password'].apply(lambda x:1 if '123' in str(x) else 0)

    df['321Seq'] = df['Password'].apply(lambda x:1 if '321' in str(x) else 0)

    df['qweSeq'] = df['Password'].apply(lambda x:1 if 'qwe' in str(x) else 0)

    df['qaSeq'] = df['Password'].apply(lambda x:1 if 'qa' in str(x) else 0)

    df['wsSeq'] = df['Password'].apply(lambda x:1 if 'ws' in str(x) else 0)

    df['pasSeq'] = df['Password'].apply(lambda x:1 if 'pas' in str(x) else 0)
    
    df['lovSeq'] = df['Password'].apply(lambda x:1 if 'lov' in str(x) else 0)
    
    df['abcSeq'] = df['Password'].apply(lambda x:1 if 'abc' in str(x) else 0)
    
    df['youSeq'] = df['Password'].apply(lambda x:1 if 'you' in str(x) else 0)

    df['countZero'] = df['Password'].apply(lambda x: str(x).count('0'))

    df['countOne'] = df['Password'].apply(lambda x: str(x).count('1'))
    
    df['countTwo'] = df['Password'].apply(lambda x: str(x).count('2'))

    df['countA'] = df['Password'].apply(lambda x: str(x).lower().count('a'))
    
    df['countE'] = df['Password'].apply(lambda x: str(x).lower().count('e'))
    
    df['countI'] = df['Password'].apply(lambda x: str(x).lower().count('i'))
    
    df['countO'] = df['Password'].apply(lambda x: str(x).lower().count('o'))
    
    df['countN'] = df['Password'].apply(lambda x: str(x).lower().count('n'))
    
    df['countR'] = df['Password'].apply(lambda x: str(x).lower().count('r'))
    
    df['countS'] = df['Password'].apply(lambda x: str(x).lower().count('s'))

    df['sequencesLength'] = df['Password'].apply(lambda x:PasswordStats(str(x)).sequences_length)

    df['repeatedPatternsLength'] = df['Password'].apply(lambda x:PasswordStats(str(x)).repeated_patterns_length)

    df['weaknessFactor'] = df['Password'].apply(lambda x:PasswordStats(str(x)).weakness_factor)

    df['entropy'] = df['Password'].apply(lambda x:PasswordStats(str(x)).entropy_bits)

    df['charCount'] = df['Password'].apply(lambda x:len(list(str(x))))

    df['numerics'] = df['Password'].apply(lambda x: len([str(x) for x in list(str(x)) if str(x).isdigit()]))
    return df

filename = 'pw_model.pkl'
model = pickle.load(open(filename, 'rb'))

def predict(password):
    password = feature_generator(password)
    return np.expm1(model.predict(password[features])[0])

@app.route('/', methods=['GET', 'POST'])
def index():
    errors = ''
    if request.method == "GET":
        return render_template('index.html')

    else:
        password = request.form['password']
        if not password:
            errors = "Заполни форму"
        else:
            prediction = predict(password)

        if not errors:
            data = {
                'password': password,
                'prediction': prediction,
            }
            return render_template("index.html", password=password, prediction=prediction)

        data = {
            'name': password,
        }

        return render_template("index.html", errors=errors, password=password)


if __name__ == '__main__':
    app.run(debug=True)